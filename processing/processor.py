from pyspark.sql.functions import (
    lit,
    udf,
    to_date,
    col,
    collect_list,
    concat,
    lower,
    struct
)

def join_and_merge_dfs(drugs, pubmed, clinical_trials):
    drugs_pubmed_join = drugs.join(pubmed, pubmed.title.contains(drugs.drug))
    drugs_clinical_trials_join = drugs.join(clinical_trials, clinical_trials.title.contains(drugs.drug))
    return drugs_pubmed_join.select('atccode', 'drug', 'id', 'date', 'journal', 'title', 'source') \
    						.union(drugs_clinical_trials_join.select('atccode', 'drug', 'id', 'date', 
        								  							 'journal', 'title', 'source')) \
    						.distinct()

def agg_pub_id_and_date_columns_as_struct(df):
    return df.withColumn('id_article_date', 
         struct(*[col('id').alias('pub_id'), 
                  col('date').alias('pub_date')]))


def collect_column_as_list(df):
    return df.select('atccode', 'id_article_date', 'source') \
             .groupBy(df.atccode, df.source) \
             .agg(collect_list(df.id_article_date).alias('article'))


def join_pubmed_and_clinical_trials_on_drug_id(df):
    pubmed = df.filter(df.source == 'pubmed').withColumnRenamed('article', 'pubmed') \
    										 .select('atccode', 'pubmed')

    clinical_trials = df.filter(df.source == 'clinical_trials') \
    					.withColumnRenamed('article', 'clinical_trials') \
    					.select('atccode', 'clinical_trials')

    return clinical_trials.join(pubmed, on=['atccode'],how='full')