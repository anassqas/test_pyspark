from processing.processor import *
from .spark_setup import SparkSetup
from pyspark.sql import Row
class Processor_test(SparkSetup):

  def test_join_and_merge_dfs(self):
    self.set_up_class()
    self.suppress_py4j_logging()

    drugs = self.spark.read.option('header', True).option("inferSchema", "true") \
                .csv('processing/test/resources/drugs.csv')
    clinical_trials = self.spark.read.option('header', True).option("inferSchema", "true") \
                          .csv('processing/test/resources/clinical_trials.csv')
    pubmed = self.spark.read.option('header', True).option("inferSchema", "true") \
                 .csv('processing/test/resources/pubmed.csv')

    results_df = join_and_merge_dfs(drugs, pubmed, clinical_trials)
    results = results_df.orderBy("id").collect()

    expected = [Row(atccode='A04AD', drug='diphenhydramine', id='1', date='01/01/2019', journal='Journal of emergency nursing', title='A 44-year-old man with erythema of the face', source='pubmed'), Row(atccode='A04AD', drug='diphenhydramine', id='2', date='01/01/2019', journal='Journal of emergency nursing', title='An evaluation of benadryl, pyribenzamine, and other so-called diphenhydramine antihistaminic drugs in the treatment of allergy.', source='pubmed'), Row(atccode='V03AB', drug='ethanol', id='6', date='2020-01-01', journal='Psychopharmacology', title='Rapid reacquisition of contextual fear following extinction in mice: effects of amount of extinction, tetracycline acute ethanol withdrawal, and ethanol intoxication.', source='pubmed'), Row(atccode='S03AA', drug='tetracycline', id='6', date='2020-01-01', journal='Psychopharmacology', title='Rapid reacquisition of contextual fear following extinction in mice: effects of amount of extinction, tetracycline acute ethanol withdrawal, and ethanol intoxication.', source='pubmed'), Row(atccode='A01AD', drug='epinephrine', id='8', date='01/03/2020', journal='The journal of allergy and clinical immunology. In practice', title='Time to epinephrine treatment is associated with the risk of mortality in children who achieve sustained ROSC after traumatic out-of-hospital cardiac arrest.', source='pubmed')]

    self.assertListEqual(results, expected)
    self.tearDown()
