import logging
import pyspark
import unittest


class SparkSetup(unittest.TestCase):

    @classmethod
    def suppress_py4j_logging(cls):
        logger = logging.getLogger('py4j')
        logger.setLevel("ERROR")

    @classmethod
    def create_testing_pyspark_session(cls):
        return (pyspark.sql.SparkSession.builder
                .master('local[*]')
                .appName('my-local-testing-pyspark-context')
                .getOrCreate())

    @classmethod
    def set_up_class(cls):
        cls.suppress_py4j_logging()
        cls.spark = cls.create_testing_pyspark_session()
        cls.spark.sparkContext.setLogLevel("ERROR")
    @classmethod
    def tear_down_class(cls):
        cls.spark.stop()
