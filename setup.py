from setuptools import setup, find_packages

PACKAGE_NAME = 'processing'
PACKAGE_VERSION = '0.0.1'
setup(
    name=PACKAGE_NAME,
    version=PACKAGE_VERSION,
    description='processing lib',
    packages=find_packages(),
    author="anass qasmi",
    author_email="anass.qas@gmail.com",
    include_package_data=True,
)
