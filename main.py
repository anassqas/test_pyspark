import re
import sys

from pyspark.context import SparkContext
from pyspark.sql import SparkSession, Window
from pyspark.sql.functions import (
    lit,
    udf,
    to_date,
    col,
    collect_list,
    concat,
    lower,
    struct
)
from pyspark.sql.types import StringType, IntegerType, DateType
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField

#join_and_merge_dfs, agg_id_and_date_columns_as_struct, collect_columns_as_list, join_pubmed_and_clinical_trials_on_drug_id

drugs_schema = StructType([StructField("atccode", StringType(),True), 
                           StructField("drug", StringType(),True)])

clinical_trials_schema = StructType([StructField("id",StringType(),True), 
                                     StructField("scientific_title",StringType(),True),
                                     StructField("date",StringType(),True), 
                                     StructField("journal",StringType(),True)])

pubmed_schema = StructType([StructField("id",IntegerType(),True), 
                            StructField("title",StringType(),True),
                            StructField("date",StringType(),True), 
                            StructField("journal",StringType(),True)])

def read_from_file(path, schema):
    return spark.read.option("header", "True").option("delimiter", ",").schema(schema).csv(path)


if __name__ == '__main__':

    print('parsing args')
    if len(sys.argv) != 5:
        raise ValueError('Please provide params.')
    
    sc = SparkContext()
    spark = SparkSession(sc)

    log4jLogger = sc._jvm.org.apache.log4j
    LOGGER = log4jLogger.LogManager.getLogger(__name__)
    LOGGER.info("pyspark logger initialized")

    drugs_path = sys.argv[1]
    clinical_trials_path = sys.argv[2]
    pubmed_path = sys.argv[3]
    output_path = sys.argv[4]

    drugs = read_from_file(drugs_path, drugs_schema).withColumn('drug', lower(col("drug")))

    clinical_trials = read_from_file(clinical_trials_path, clinical_trials_schema) \
                        .withColumn('title', lower(col("scientific_title"))) \
                        .withColumn('source', lit("clinical_trials")) \
                        .drop('scientific_title')

    pubmed = read_from_file(pubmed_path, pubmed_schema) \
                .withColumn('title', lower(col("title"))) \
                .withColumn('source', lit("pubmed")) 

    from processing import * 

    merged_df = processor.join_and_merge_dfs(drugs, pubmed, clinical_trials)

    df = processor.agg_pub_id_and_date_columns_as_struct(merged_df)

    df = processor.collect_column_as_list(df)

    joined_df = processor.join_pubmed_and_clinical_trials_on_drug_id(df)

    joined_df.coalesce(1).write.json(output_path)


    
